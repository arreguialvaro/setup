#!/bin/bash
git config --global user.email "arreguialvaro@gmail.com"
git config --global user.name "Alvaro Arregui"

# Install OhMyZsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

git clone https://github.com/powerline/fonts.git --depth=1
./fonts/install.sh
rm -rf fonts

# Download TunnelBear VPN config
wget "https://s3.amazonaws.com/tunnelbear/linux/openvpn.zip"
unzip openvpn.zip
rm openvpn.zip

# Install NVM
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
echo 'export NVM_DIR="$HOME/.nvm"' >> ~/.zshrc
echo '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"' >> ~/.zshrc
echo "DEFAULT_USER=`whoami`" >> ~/.zshrc
echo "export VISUAL=vim" >> ~/.zshrc
echo 'export EDITOR="$VISUAL"' >> ~/.zshrc

source ~/.zshrc
nvm install 8.11.1
nvm use 8.11.1
nvm alias default 8.11.1

npm i -g yarn
npm i -g nodemon

code --install-extension 2gua.rainbow-brackets
code --install-extension dbaeumer.vscode-eslint
code --install-extension formulahendry.auto-close-tag
code --install-extension leizongmin.node-module-intellisense
code --install-extension mohsen1.prettify-json
code --install-extension robertohuertasm.vscode-icons
code --install-extension wayou.vscode-todo-highlight
